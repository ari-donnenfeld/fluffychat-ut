/*
 * Copyright (C) 2023  red
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * fluffychatflutter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtWebEngine 1.10
import Lomiri.PushNotifications 0.1
 
import QtQuick.Controls 2.1
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.3
import Lomiri.DownloadManager 1.2
import QtQuick.Window 2.12
import QtQml 2.12

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'fluffychatflutter.red'
    automaticOrientation: true
    anchors {
        fill : parent
        bottomMargin : LomiriApplication.inputMethod.visible
            ? LomiriApplication
                .inputMethod
                .keyboardRectangle
                .height / Screen.devicePixelRatio
            : 0
        Behavior on bottomMargin {
            NumberAnimation {
                duration : 175
                easing.type : Easing.OutQuad
            }
        }
    }

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent


        header: WebEngineView {
            id: webview
            anchors.fill: parent
            focus: true
            zoomFactor: units.gu(1) / 8.4
            settings.pluginsEnabled: false

            // Test settings
            settings.accelerated2dCanvasEnabled: true
            settings.javascriptEnabled : true
            settings.allowRunningInsecureContent: true
            settings.javascriptCanAccessClipboard: true
            settings.javascriptCanPaste: true
            settings.localContentCanAccessFileUrls: true
            settings.localContentCanAccessRemoteUrls: true
            settings.localStorageEnabled: true
            settings.webGLEnabled: true

            
            url: "http://0.0.0.0:8100"

            profile:  WebEngineProfile {
                id: webContext
                httpUserAgent: "Mozilla/5.0 (Linux; Ubuntu 20.04 like Android 9) AppleWebKit/537.36 Chrome/87.0.4280.144 Mobile Safari/537.36"
                storageName: "Storage"
                // persistentStoragePath: "/home/phablet/.cache/fluffychatflutter.red/QtWebEngine"
                persistentStoragePath : "/home/phablet/.local/share/fluffychatflutter.red/QWebEngine"
            }

            Component.onCompleted: {
                print("I'm printed right away..")
            }

            onFileDialogRequested: function(request) {
                request.accepted = true;
                var importPage = mainPageStack.push(Qt.resolvedUrl("ImportPage.qml"),{"contentType": ContentType.All, "handler": ContentHandler.Source})
                importPage.imported.connect(function(fileUrl) {
                    console.log(String(fileUrl).replace("file://", ""));
                    request.dialogAccept(String(fileUrl).replace("file://", ""));
                    mainPageStack.push(pageMain)
                })
            }
            onNewViewRequested: {
                request.action = WebEngineNavigationRequest.IgnoreRequest
                if(request.userInitiated) {
                    Qt.openUrlExternally(request. requestedUrl)
                }
            }
            onFeaturePermissionRequested: function(url, feature) {
                print(url, feature)
                grantFeaturePermission(url, feature, true)
            }
        }
    }
    PushClient {
        id: pushClient
        appId: "fluffychatflutter.red_fluffychatflutter"

        onTokenChanged:  {
            webview.runJavaScript('utToken = "' + pushClient.token + '"')
            console.log("👍PushClient:", pushClient.token)
            webview.runJavaScript('console.log("Running this! FINDME")')
        }
    }
}
