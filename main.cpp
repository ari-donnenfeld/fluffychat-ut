/*
 * Copyright (C) 2023  red
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * fluffychatflutter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>
#include <thread>
#include "httplib.h"


int startServer() {
    
    qDebug() << "Inside the startServer";
    
    using namespace httplib;
    Server svr;

    svr.set_base_dir("www");
    /* auto ret = svr.set_mount_point("/", "./www"); */

    svr.set_logger([](const auto& req, const auto& res) {
        /* qDebug() << "Sometrhing"; */
        /* qDebug() << res; */
    });
    svr.set_error_handler([](const auto& req, auto& res) {
        /* qDebug() << "There is an error"; */
    });
    
    qDebug() << "Just before listen";

    svr.listen("0.0.0.0", 8100);
    
    qDebug() << "I've started the web server";

    return 0;
}

int main(int argc, char *argv[])
{
    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    app->setApplicationName("fluffychatflutter.red");

    qDebug() << "Starting app from main.cpp";
    
    std::thread th1(startServer);
    /* startServer(); */

    qDebug() << "I've created the thread";

    QQuickView *view = new QQuickView();
    view->setSource(QUrl("qrc:/Main.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    
    qDebug() << "Execing the application";
    app->exec();
    qDebug() << "Just before join";
    th1.join();
    qDebug() << "right after join";
    
    return 0; 
}
